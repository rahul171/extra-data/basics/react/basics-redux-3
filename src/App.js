import React from 'react';
import { increase, decrease, increaseCustom, increaseCustomAsync, message } from './appSlice';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

class App extends React.Component {
  componentDidUpdate(prevProps, prevState, snapshot) {
    // console.log('componentDidUpdate prevMessage =>', prevProps.message, '- message =>', this.props.message);
  }

  render() {
    // console.log('called', this.props);
    console.log(increase({data: 'data'}))
    console.log(message({data: 'data'}))

    return (
      <div>
        <div>
          <div>{this.props.value}</div>
          <div>{this.props.message}</div>
        </div>
        <hr />
        <div>
          <div>
            {/*<button onClick={this.props.actions.increase}>increase</button>*/}
            <button onClick={this.props.increase}>increase</button>
          </div>
          <div>
            {/*<button onClick={this.props.actions.decrease}>decrease</button>*/}
            <button onClick={this.props.decrease}>decrease</button>
          </div>
          <div>
            {/*<button onClick={this.props.actions.increaseCustom}>increase custom</button>*/}
            <button onClick={this.props.increaseCustom}>increase custom</button>
          </div>
          <div>
            {/*<button onClick={this.props.actions.increaseCustomAsync}>increase custom async</button>*/}
            <button onClick={this.props.increaseCustomAsync}>increase custom async</button>
          </div>
        </div>
        <div>
          {Array(10).fill(1).map((item, index) => (
            <button key={index}>{this.props.message}</button>
          ))}
        </div>
      </div>
    );
  }
}

// try with second argument, see how many times this function gets called
const mapStateToProps = (store) => {
  // console.log('mapStateToProps');

  return {
    value: store.app.value,
    message: store.app.message
  };
};

// try with second argument, see how many times this function gets called
// const mapDispatchToProps = (dispatch) => {
//   console.log('mapDispatchToProps');
//
//   return {
//     actions: {
//       increase: () => dispatch(increase()),
//       decrease: () => dispatch(decrease()),
//       increaseCustom: () => dispatch(increaseCustom({ value: Math.floor(Math.random() * 101) })),
//       increaseCustomAsync: () => dispatch(increaseCustomAsync(Math.floor(Math.random() * 101)))
//     }
//   };
// };

// const mapDispatchToProps = { increase, decrease, increaseCustom, increaseCustomAsync };

// const mapDispatchToProps = (dispatch) => ({
//   ...bindActionCreators({ increase, decrease, increaseCustom, increaseCustomAsync }, dispatch)
// });

const mapDispatchToProps = {
  increase: () => increase(),
  decrease: () => decrease(),
  increaseCustom: () => increaseCustom({ value: Math.floor(Math.random() * 101) }),
  increaseCustomAsync: () => increaseCustomAsync(Math.floor(Math.random() * 101))
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
