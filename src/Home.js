import React, {useState} from 'react';
import App from './App';

const Home = (props) => {
  const [value, setValue] = useState(0);

  return (
    <React.Fragment>
      {Math.random() > 0 && <App value={value} />}
      <button onClick={() => setValue(Math.random())}>update value</button>
    </React.Fragment>
  );
}

export default Home;
