import {createSlice} from '@reduxjs/toolkit';

const appSlice = createSlice({
  name: 'app',
  initialState: {
    value: 0,
    message: ''
  },
  reducers: {
    increase: state => { state.value++ },
    decrease: state => ({ value: state.value - 1, message: 'hello' }),
    increaseCustom: (state, action) => {
      console.log('state', state);
      console.log('action', action);
      state.value += action.payload.value;
    },
    message: {
      reducer: (state, action) => {
        console.log(action);
        state.message = action.payload
      },
      prepare: (...args) => {
        console.log('inside prepare', args);
        return {
          payload: args[0],
          meta: { a: 'metadata' },
          error: true
        }
      }
    }
  }
});

export const { increase, decrease, increaseCustom, message } = appSlice.actions;

export const increaseCustomAsync = (value, time = 3000) => async (dispatch, useState) => {
  dispatch(message('waiting for the data...'));

  await new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve();
    }, time);
  });

  // console.log('calling 1')
  dispatch(increaseCustom({ value }));
  // console.log('calling 2')
  dispatch(message('efdf'));
  // console.log('called both')
}

export const selectValue = state => state.value;

export default appSlice.reducer;
